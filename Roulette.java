import java.util.Scanner;

public class Roulette {
    public static void main(String[] args) {
        RouletteWheel rouletteWheel = new RouletteWheel();
        Scanner scan = new Scanner(System.in);
        int userMoney = 1000;

        while (userMoney > 0) {
            System.out.println("Do you want to make a bet? (yes/no): ");
            String answer = scan.nextLine();

            if (answer.equals("no")) {
                System.out.println("Thanks for playing!");
                break;
            }

            if (answer.equals("yes")) {
                System.out.println("You have $" + userMoney);
                System.out.println("Enter the amount you want to bet: ");
                int betMoney = Integer.parseInt(scan.nextLine());

                    if (betMoney <= userMoney) {
                    System.out.println("Enter the number you want to bet on (0-36): ");
                    int betNumber = Integer.parseInt(scan.nextLine());

                            if (betNumber > 36 || betNumber < 0) {
                            System.out.println("The number is out of the wheel's range!");
                            continue; 
                            }

                    rouletteWheel.spin();
                    int spinResult = rouletteWheel.getValue();

                    if (spinResult == betNumber) {
                        int win = betMoney * 35;
                        userMoney += win;
                        System.out.println("Congratulations! You won $" + win);
                    } else {
                        userMoney = userMoney - betMoney;
                        System.out.println("Sorry, you lost $" + betMoney);
                    }
                    } 
                    
                    else {
                    System.out.println("You don't have enough money to make that bet.");
                    }
                }
        }

        System.out.println("Game over. You finished with $" + userMoney);

    }
}


