import java.util.Random;

public class RouletteWheel{
    private Random random;
    private int lastSpin;

    public RouletteWheel(){
        random = new Random();
        lastSpin = 0;
    }

    //update and store new random int in between 0-37
    public void spin(){
        lastSpin = random.nextInt(37);

    }

    //retrun the int of the last spin
    public int getValue() {
        return lastSpin;
    }

}